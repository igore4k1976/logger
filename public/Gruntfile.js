/*jslint node: true */
'use strict';

var pkg = require('./package.json');

function isFilePartOfServerMock(src) {
    var root = pkg.isLinux ? src.split('\/')[0] : src.split('\\')[0];
    return root.toLowerCase() === 'server-mocks';
}

function isFilePartOfAppCode(src) {
    var root = pkg.isLinux ? src.split('\/')[0] : src.split('\\')[0];
    return root.toLowerCase() === 'app';
}

//Using exclusion patterns slows down Grunt significantly
//instead of creating a set of patterns like '**/*.js' and '!**/node_modules/**'
//this method is used to create a set of inclusive patterns for all subdirectories
//skipping node_modules, bower_components, dist, and any .dirs
//This enables users to create any directory structure they desire.
var createFolderGlobs = function (fileTypePatterns, localIgnores) {
    fileTypePatterns = Array.isArray(fileTypePatterns) ? fileTypePatterns : [fileTypePatterns];
    var ignore = ['node_modules', 'bower_components', 'dist', 'temp', 'test', 'data', 'assets', 'libs'];


    if (Array.isArray(localIgnores)) {
        ignore = ignore.concat(localIgnores);
    }

    var fs = require('fs');
    return fs.readdirSync(process.cwd())
        .map(function (file) {
            if (ignore.indexOf(file) !== -1 || file.indexOf('.') === 0 || !fs.lstatSync(file).isDirectory()) {
                return null;
            } else {
                return fileTypePatterns.map(function (pattern) {
                    return file + '/**/' + pattern;
                });
            }
        })
        .filter(function (patterns) {
            return patterns;
        })
        .concat(fileTypePatterns);
};

module.exports = function (grunt) {

    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    console.log('Building TPS application (include fake server: ' + pkg.fakeServer + ')');

    // Project configuration.
    grunt.initConfig({
        pkg: pkg,
        connect: {
            main: {
                options: {
                    port: 9002,
                    open: {
                        target: 'http://localhost:9002',
                        appName: 'Chrome'
                    }
                }
            }
        },
        watch: {
            main: {
                options: {
                    livereload: 35730,
                    livereloadOnError: false,
                    spawn: false
                },
                files: [createFolderGlobs(['*.js', '*.html']), '!_SpecRunner.html', '!.grunt'],
                tasks: [] //all the tasks are run dynamically during the watch event handler
            }
        },
        jshint: {
            main: {
                options: {
                    jshintrc: '.jshintrc'
                },
                src: ['app/**/*.js']
            }
        },
        clean: {
            before: {
                src: ['dist', 'temp']
            },
            after: {
                src: ['temp']
            }
        },
        ngtemplates: {
            main: {
                options: {
                    module: 'tps',
                    url: function (url) {
                        return url;//.replace('app/', '');
                    }
                },
                src: [createFolderGlobs('*.html'), '!index.html', '!_SpecRunner.html'],

                dest: 'temp/templates.js'
            }
        },
        copy: {
            main: {
                files: [
                    {
                        src: ['<%= dom_munger.data.appcss %>'],
                        dest: 'dist',
                        filter: function (filename) {
                            return filename.match(/[.]min[.]css/);
                        },
                        expand: true

                    },
                    {
                        cwd: 'app',
                        src: ['data/**/*'],
                        dest: 'dist/app',
                        expand: true
                    },
                    {
                        cwd: 'app',
                        src: ['assets/**/*', '!assets/**/*.css'],
                        dest: 'dist/app',
                        expand: true
                    },
                    {
                        cwd: 'app',
                        src: ['lang/**/*'],
                        dest: 'dist/app',
                        expand: true
                    }
                ]
            }
        },
        dom_munger: { // jshint ignore:line
            read: {
                options: {
                    read: [
                        {
                            selector: 'script[data-concat!="false"]',
                            attribute: 'src',
                            writeto: 'appjs',
                            isPath: true
                        },
                        {
                            selector: 'link[rel="stylesheet"][data-concat!="false"]',
                            attribute: 'href',
                            isPath: true,
                            writeto: 'appcss'
                        }
                    ]
                },
                src: 'index.html'
            },
            update: {
                options: {
                    remove: ['script[data-remove!="false"]'],
                    append: [
                        {
                            selector: 'body',
                            html: '<script src="vendors.full.min.js"></script>'
                        },
                        {
                            selector: 'body',
                            html: '<script src="app.full.min.js"></script>'
                        }

                    ],
                    callback: function ($) {
                        // rebase all css files to root
                        $('link[href^="../"]').each(function (i, element) {
                            var newHref = $(element).attr('href').replace(/^[.][.]\//i, '');
                            $(element).attr('href', newHref);
                        });

                        // change suffix of all css files to min.css
                        $('link[href$=".css"]:not([href$="min.css"])').each(function (i, element) {
                            var newHref = $(element).attr('href').replace(/[.]css$/i, '.min.css');
                            $(element).attr('href', newHref);
                        });

                    }
                },
                src: 'index.html',
                dest: 'dist/index.html'
            }
        },
        cachebreaker: {
            dev: {
                options: {
                    match: ['[.]js', '[.]css'],
                },
                files: {
                    src: ['dist/index.html']
                }
            }
        },
        cssmin: {
            main: {
                expand: true,
                src: ['<%= dom_munger.data.appcss %>', '!**/*.min.css'],
                dest: 'dist/app',
                rename: function (dist, src) {

                    var path = require('path');

                    var newSrc = src.trim();

                    if (newSrc.match(/.css$/i) && !newSrc.match(/.min.css$/i)) {
                        newSrc = newSrc.replace(/.css$/i, '.min.css');
                    }

                    if (newSrc.match(/^app\//i)) {
                        newSrc = newSrc.replace(/^app\//i, '');
                    }

                    return path.join(dist, newSrc);
                }

            }
        },
        concat: {
            appjs: {
                src: ['<%= dom_munger.data.appjs %>', '<%= ngtemplates.main.dest %>'],
                dest: 'temp/app.full.js',
                filter: function (src) {


                    // concat to app only app code and if required also mock code
                    var expression = pkg.isLinux ? '^temp\/' : '^temp';
                    var include = src.match(new RegExp(expression, 'i')) || (pkg.fakeServer && isFilePartOfServerMock(src)) || isFilePartOfAppCode(src);

                    console.log((include ? 'include - ' : 'exclude - ') + src);
                    return include;
                }
            },
            vendorsjs: {
                src: ['<%= dom_munger.data.appjs %>'],
                dest: 'dist/vendors.full.min.js',
                filter: function (src) {
                    // concat to vendor only javascript files not part of app code
                    return !isFilePartOfServerMock(src) && !isFilePartOfAppCode(src);
                }
            }
        },
        'string-replace': {
            fakeServer: {
                options: {
                    replacements: [{
                        pattern: /'tpsServerMocks',/i,
                        replacement: ''
                    }]
                },
                src: 'temp/app.full.js',
                dest: 'temp/app.full.js',
                filter: function (src) {
                    if (pkg.fakeServer) {
                        return false;
                    } else {
                        return true;
                    }

                }
            },
            disableAngularDebugInfo: {
                options: {
                    replacements: [{
                        pattern: 'enableAngularDebugInfo: true,',
                        replacement: 'enableAngularDebugInfo: false,'
                    }
                    ]
                },
                src: 'temp/app.full.js',
                dest: 'temp/app.full.js'
            },
            enableReloadOnSelect: {
                options: {
                    replacements: [{
                        pattern: 'disableOnClickTabRefresh: true,',
                        replacement: 'disableOnClickTabRefresh: false,'
                    }
                    ]
                },
                src: 'temp/app.full.js',
                dest: 'temp/app.full.js'
            },
            disableLogs: {
                options: {
                    replacements: [{
                        pattern: 'enableApplicationLogging: true,',
                        replacement: 'enableApplicationLogging: false,'
                    }, {
                        pattern: 'enableServerRequestsLogging: true,',
                        replacement: 'enableServerRequestsLogging: false,'
                    }, {
                        pattern: 'developmentMode: true,',
                        replacement: 'developmentMode: false,'
                    }
                    ]
                },
                src: 'temp/app.full.js',
                dest: 'temp/app.full.js'
            }
        },
        ngAnnotate: {
            main: {
                src: 'temp/app.full.js',
                dest: 'temp/app.full.js'
            }
        },
        uglify: {
            options: {
                mangle: false // NOTE : this value is a must, otherwise Angular modules will not function
            },
            main: {
                src: 'temp/app.full.js',
                dest: 'dist/app.full.min.js'
            }
        },
        htmlmin: {
            main: {
                options: {
                    collapseBooleanAttributes: true,
                    collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeComments: true,
                    removeEmptyAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true
                },
                files: {
                    'dist/index.html': 'dist/index.html'
                }
            }
        },

        /**
         * The Karma configurations.
         */
        karma: {
            unit: {
                files: [
                    {
                        pattern: '<%= dom_munger.data.appjs %>'
                    },
                    {
                        pattern: "app/**/*-spec.js"
                    }
                ],
                configFile: 'test/karma.conf.js',
                singleRun: true
            },
            duringWatch: {
                files: [], // the files will be created dynamically durint the 'watch' event
                configFile: 'test/karma.conf.js',
                singleRun: true
            }

        },

        /**
         * Protractor configuration
         */
        protractor: {
            options: {
                keepAlive: true,
                configFile: "test/protractor.conf.js"
            },
            singlerun: {
                keepAlive: false
            },
            auto: {
                keepAlive: false,
                options: {
                    args: {
                        seleniumPort: 4444
                    }
                }
            }
        },
        shell: {
            xvfb: {
                command: 'Xvfb :99 -ac -screen 0 1600x1200x24',
                options: {
                    async: true
                }
            }
        },
        env: {
            xvfb: {
                DISPLAY: ':99'
            }
        }

    });

    grunt.registerTask('build', ['jshint', 'clean:before', 'dom_munger', 'ngtemplates', 'cssmin', 'concat', 'ngAnnotate', 'string-replace', 'uglify', 'copy', 'cachebreaker', 'htmlmin', 'clean:after']);
    grunt.registerTask('run', ['dom_munger:read', 'jshint', 'connect', 'watch']);
    grunt.registerTask('test', ['dom_munger:read', 'karma:unit']);
    grunt.registerTask('e2e', ['dom_munger:read', 'shell:xvfb', 'env:xvfb', 'protractor:singlerun', 'shell:xvfb:kill']);
    grunt.registerTask('e2e-visual', ['dom_munger:read', 'protractor:singlerun' ]);
    grunt.registerTask('jscheck', ['jshint']);
    grunt.registerTask('dev', ['dom_munger:read', 'watch']);

    // currently grunt does not allow functions in tasks chains, so we need to register a task to update config of karma before executing the unit test
    grunt.registerTask('test-only', ['dom_munger:read', 'test-only-specified-files']);
    grunt.registerTask('test-only-specified-files', function () {

        // get the file option
        var testedFile = grunt.option('file');

        // if file not found, log error and kill the process
        if (!grunt.file.exists(testedFile)) {
            console.error("Couldn't find " + testedFile);
            process.exit();
        }

        // read the dom_munger appjs (all the js files of the app)
        var src = [].concat(grunt.config('dom_munger.data.appjs'));
        src.push('bower_components/angular-mocks/angular-mocks.js');
        src.push(testedFile);

        // configure karma:unit in runtime to run only the requested src
        grunt.config('karma.unit.files', [{ src: src }]);

        // run unit test
        grunt.task.run(['karma:unit']);

    });

    grunt.event.on('watch', function (action, filepath) {
        //https://github.com/gruntjs/grunt-contrib-watch/issues/156

        var tasksToRun = [];

        if (filepath.lastIndexOf('.js') !== -1 && filepath.lastIndexOf('.js') === filepath.length - 3) {

            //lint the changed js file
            grunt.config('jshint.main.src', filepath);
            tasksToRun.push('jshint');

            //find the appropriate unit test for the changed file
            var spec = filepath;
            if (filepath.lastIndexOf('-spec.js') === -1 || filepath.lastIndexOf('-spec.js') !== filepath.length - 8) {
                spec = filepath.substring(0, filepath.length - 3) + '-spec.js';
            }

            //if the spec exists then lets run it
            if (grunt.file.exists(spec)) {
                var src = [].concat(grunt.config('dom_munger.data.appjs'));
                src.push('bower_components/angular-mocks/angular-mocks.js');
                src.push(spec);
                grunt.config('karma.duringWatch.files', [
                    {
                        src: src
                    }
                ]);
                tasksToRun.push('karma:duringWatch');
            }
        }

        //if index.html changed, we need to reread the <script> tags so our next run of karma
        //will have the correct environment
        if (filepath === 'index.html') {
            tasksToRun.push('dom_munger:read');
        }

        grunt.config('watch.main.tasks', tasksToRun);

    });

};