/**
 * Created by Sandeep on 01/06/14.
 */
angular.module('loggerApp.controllers',[]).controller('loggerController',function($scope,loggerService,$q,loggerConfig,utilsService){



    $scope.timeQuery = moment().format("YYYY-MM-DD HH:mm:ss");
    $scope.timeRange =  _.find(loggerConfig.timeRange, function(log){return  log.defualt});
    $scope.gridRebindVersion = 1;
    $scope.requestId = null;
    $scope.rangeOptions = {
        dataSource: loggerConfig.timeRange,
        dataTextField: "text",
        dataValueField: "value"
    };

    $scope.timeOption = {
        timeFormat: 'HH:mm:ss',
        format: 'yyyy-MM-dd HH:mm:ss',
        value:new Date()
    };



    $scope.serversOptions = {
        checkboxes: {
            checkChildren: true
        },
        dataSource: loggerConfig.servers
    }


    $scope.applications = [];
    function gatherStates(nodes,applications) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked && !nodes[i].hasChildren) {
                applications.push(nodes[i].id);
            }

            if (nodes[i].hasChildren) {
                gatherStates(nodes[i].children.view(),applications);
            }
        }
    }

    $scope.logscollected = [];


    $scope.runQuery = function(){

        var self = this;
		$scope.applications=[];
        gatherStates( $("#serversTree").data("kendoTreeView").dataSource.view(),$scope.applications);
        var params = {
            "applications": $scope.applications,
            "dateAndTime": moment(self.timeQuery).format("YYYY-MM-DD HH:mm:ss.SSS"),
            "timeWindowSizeSeconds": self.timeRange.value
        };

        self.requestId =  utilsService.generateGUID();

        _.extend(params, {"reqId": self.requestId});

        loggerService.collectLog(params).then(
            function (result) {
                //unselect all selected checkbox
                $("#serversTree").data("kendoTreeView").element.find("input[type=checkbox]").prop("checked", false);
                $scope.applications=[];

                self.logscollected.push({
                    timestamp:self.timeQuery,
                    timeRange:self.timeRange.text,
                    requestId: self.requestId,
                    linkShow: false
                });

                loggerService.pullToGetLogsCollected(self.requestId).then(
                    function (result) {
                        _.each(self.logscollected, function(log,index){
                            if(log.requestId === result.requestId){
                                utilsService.safeApply(self,function(){
                                    log.linkShow = true;
                                });
                            }
                        });
                    },
                    function (reason) {
                    }
                );
            },
            function (reason) {

            }
        );
    }

    $scope.downloadLogs = function(requestId){
        loggerService.downloadLogs(requestId).then(
            function (result) {
                // self.hideLoading();
            },
            function (reason) {
                //self.showError(tpsErrorHandlerService.getErrorMessage(reason.error));
            }
        );
    }


    $scope.showMergedLog = function(requestId){
       // $scope.gridOptions = createGridOptions();
        $("#logsGrid").data("kendoGrid").dataSource.read({requestId:requestId});
        $("#logsGrid").data("kendoGrid").refresh();
    }


    $scope.gridOptions = {
        dataSource: new kendo.data.DataSource({
            transport: {
                data: {
                    requestId: "requestId"
                },
                read: function (e) {
                    $q.when(loggerService.getMergedLog(e.data.requestId)).then(
                        function (result) {
                            if (angular.isUndefined(result)) {
                                e.success({
                                    total: 0,
                                    items: []
                                });
                            } else {
                                e.success({
                                    total: result.length,
                                    items: result
                                })
                            }
                        },
                        function (reason) {
                            e.success({
                                total: 0,
                                items: []
                            });
                        }
                    );
                }
            },
            schema: {
                data: "items",
                total: "total", // total is returned in the "total" field of the response
                model: {
                    fields: {
                        "server": {
                            "type": "string"
                        },
                        "app": {
                            "type": "string"
                        },
                        "timestamp": {
                            "type": "string"
                        },
                        "msg": {
                            "type": "string"
                        }
                    }
                }
            },
            pageSize: 10,
            serverPaging: false,
            serverFiltering: false,
            serverSorting: false,
            serverGrouping: false
        }),
        autoBind:false,
        sortable: true,
        pageable: true,
        reorderable: false,
        groupable: false,
        resizable: true,
        dataBound: function() {
         },
        columnMenu: true,
        pageable: {
            pageSizes: 10,
            // buttonCount: gridPageSize,
            messages: {
                display: "Showing {0}-{1} results out of {2}"
            }
        },
        columns: [{
            field: "server",
            title: "SERVER",
            width: "10%"
        },
            {
                field: "app",
                title: "APP",
                width: "10%"
            },
            {
                field: "timestamp",
                title: "TIME",
               // format:"YYYY-MM-DD HH:mm:ss",
                width: "15%"
            },
            {
                field: "msg",
                title: "MESSAGE",
                width: "65%",
                filterable: false,
                sortable: false
            }]
    };



});