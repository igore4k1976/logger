/**
 * Created by Sandeep on 01/06/14.
 */

angular.module('loggerApp.services',[]).service('loggerService',function($http,$q,$timeout,loggerConfig){

    function collectLog(params) {

        var deferred = $q.defer();
        $http.post('uicollectlog',params)
            .success(function(result){
                deferred.resolve();
            })
            .error(function(){
                deferred.reject();
            });

        return deferred.promise;
    }

    function pullToGetLogsCollected(requestId){
        var deferred = $q.defer();
        var timer = null;
        function pullRequestToGetLogsCollected(requestId){
            getLogsCollected(requestId).then(
                function (result) {
                    $timeout.cancel( timer );
                    deferred.resolve({requestId:requestId});
                },
                function (reason) {
                     timer = $timeout(function() {
                        pullRequestToGetLogsCollected(requestId);
                     }, loggerConfig.pullingTimeout);
                });
        }

        pullRequestToGetLogsCollected(requestId);

        return deferred.promise;
    }


    function getMergedLog(requestId) {

        var deferred = $q.defer();
            $http.get('uigetmergedlog/' +requestId)
                .success(function(result){
                        deferred.resolve(result['mergedEntries']);
                })
                .error(function(){
                    deferred.reject();
                });

        return deferred.promise;
    }

    function downloadLogs(requestId) {

        var deferred = $q.defer();
        $http.get('uidownloadlogs/' +requestId)
            .success(function(result){
                deferred.resolve();
            })
            .error(function(){
                deferred.reject();
            });

        return deferred.promise;
    }

    function getLogsCollected(requestId) {

        var deferred = $q.defer();
        $http.get('uilogscollected/' +requestId)
            .success(function(result){
                deferred.resolve();
            })
            .error(function(){
                deferred.reject();
            });

        return deferred.promise;
    }

    function downloadLogs(requestId) {


        var deferred = $q.defer();

        // create timer to remove the pending download whether it was finished or not.
        var cleanupDownload = $timeout(function () {
            $timeout(removeDownloadIframe(), 100);
        }, 60000);


        // run the download plugin
        $.fileDownload('uidownloadlogs/' +requestId, {
            successCallback: downloadDone,
            failCallback: downloadFailed
        });


        /**
         * Download success callback
         */
        function downloadDone() {

            // cancel the cleanup and remove iframe and resolve.
            $timeout.cancel(cleanupDownload);

            $timeout(removeDownloadIframe(), 100);

            deferred.resolve();
        }

        /**
         * Failed download callback, responseHtml send by the plugin
         * @param responseHtml
         */
        function downloadFailed(responseHtml) {

            // build error object
            var errorObj = {
                error: {
                    appId: 'download-file-error',
                    serverId: null
                }
            };

            if (responseHtml){

                var errorMessage = JSON.parse($.parseHTML(responseHtml)[0].innerText);

                errorObj = {
                    error: {
                        serverId: errorMessage ? errorMessage.error : 404
                    }
                };
            }

            // cancel the timer for cleanup and remove iframe, then reject promise.
            $timeout.cancel(cleanupDownload);

            $timeout(removeDownloadIframe(), 100);

            deferred.reject(errorObj);
        }

        /**
         * Remove download iframe
         */
        function removeDownloadIframe() {

            var downloadIframe = $('body').find('iframe').filter("[src='uidownloadlogs/" +requestId+"']");

            if (downloadIframe) {
                downloadIframe[0].remove();
            }

            downloadIframe = null;

        }

        return deferred.promise;
    }



    return {
        getMergedLog:getMergedLog,
        downloadLogs:downloadLogs,
        collectLog:collectLog,
        pullToGetLogsCollected:pullToGetLogsCollected
    }
});