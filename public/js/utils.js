/**
 * Created by EMatok on 2/16/2016.
 */
/**
 * Created by Sandeep on 01/06/14.
 */

angular.module('loggerApp.utils',[]).service('utilsService',function($http,$q){

    function generateGUID() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    function safeApply(scope, fn) {
        if (scope.$$phase || scope.$root && scope.$root.$$phase) {

            fn();
        } else {
            scope.$apply(fn);
        }
    }


    return {
        generateGUID:generateGUID,
        safeApply:safeApply
    }
});