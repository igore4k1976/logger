/**
 * Created by Sandeep on 01/06/14.
 */

angular.module('loggerApp',['ui.router','ngResource','loggerApp.controllers','loggerApp.services','loggerApp.utils','kendo.directives']);

angular.module('loggerApp').config(function($stateProvider,$httpProvider){
    $stateProvider.state('/',{
        url:'/',
        templateUrl:'views/logger.html',
        controller:'loggerController'
    });
}).run(function($state){
   $state.go('/');
});