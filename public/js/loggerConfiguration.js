(function () {
    angular.module('loggerApp').constant('loggerConfig', {
        timeRange:[
            {'value':1,"text":'1 second', "defualt":true},
            {'value':5,"text":'5 seconds'},
            {'value':10,"text":'10 seconds'},
            {'value':20,"text":'20 seconds'},
            {'value':30,"text":'30 seconds'},
            {'value':60,"text":'1 minute'},
            {'value':600,"text":'10 minutes'}
        ],
        pullingTimeout:10000,
        servers:[
            {
                text:"DDP",
                expanded:true,
                items:[
                    {"id":"Sniffer","text":"Sniffer"},
                    {"id":"File Monitor","text":"File Monitor"}
                ]
            },
            {
                text:"CNC",
                expanded:false,
                items:[
                    {"id":"TOP","text":"TOP"},
                    {"id":"DAP","text":"DAP"},
                    {"id":"LAP0","text":"LAP0"},
                    {"id":"LAP1","text":"LAP1"},
                ]
            },
            {
                text:"MFD",
                expanded:false,
                items:[
                    {"id":"Manager","text":"Manager"}
                ]
            },
            {
                text:"Brain",
                expanded:false,
                items:[
                    {"id":"Brain","text":"Brain"}
                ]
            },
            {
                text:"GUI",
                expanded:false,
                items:[
                    {"id":"GUI","text":"GUI"}
                ]
            }
        ]


    });
})();