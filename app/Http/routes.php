<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

//$app->post('/uicollectlog', function () use ($app) {
//    return 'qweqweqweqw';
//});

$app->post('uicollectlog', [
    'uses' => 'ExampleController@uicollectlog'
]);

$app->get('uilogscollected/{action_id}', [
    'uses' => 'ExampleController@getuicollectlog'
]);

$app->get('uidownloadlogs/{action_id}', [
    'uses' => 'ExampleController@gdownloaduicollectlog'
]);

$app->get('uigetmergedlog/{requestId}', [
    'uses' => 'ExampleController@uigetmergedlog'
]);

